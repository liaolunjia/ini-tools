# ini tools

## How to use

## Demo

### 创建 ini 文件 `config_demo.ini`

```ini
[section_1]
option_1 = 111
option_2 = 222.23456789

[section_a]
name = LLJ
```

### 创建 `main.cpp`

```c++
#include <iostream>

#include "ini_tools/exception/INIReaderException.h"
#include "ini_tools/ini_config.h"
#include "ini_tools/ini_parse.h"

int main() {
  ini_tools::IniParse ini_parse;
  ini_tools::IniConfig ini_config;
  try {
    ini_config = ini_parse.read("config_demo.ini");
    //  获取所有sections
    auto sections = ini_config.sections();
    for (const auto& item : sections) {
      std::cout << item << std::endl;
    }

    int s1_o1;
    ini_config.getOption("section_1", "option_1", s1_o1);
    std::cout << "section_1_option_1: " << s1_o1 << std::endl;

    double s1_o2;
    ini_config.getOption("section_1", "option_2", s1_o2);
    printf("section_1_option_2: %.7f \r\n", s1_o2);

    std::string sa_name;
    ini_config.getOption("section_a", "name", sa_name);
    std::cout << "section_a_option_name: " << sa_name << std::endl;
  } catch (const std::exception& e) {
    std::cout << e.what() << std::endl;
  }

  try {
    ini_parse.write(ini_config, "write_demo1.ini");
  } catch (const ini_tools::INIReaderException& e) {
    std::cout << e.what() << std::endl;
  }

  return 0;
}
```

output

```output
section_a
section_1
section_1_option_1: 111
section_1_option_2: 222.2345679 
section_a_option_name: LLJ
```