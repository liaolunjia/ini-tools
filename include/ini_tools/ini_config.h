/*******************************************************************************
 * Copyright (c) 2022/09/09, Liao LunJia.
 * All rights reserved.
 *******************************************************************************/
#pragma once

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <sstream>
#include <unordered_map>
#include <utility>
#include <vector>

#include "exception/DuplicateOptionException.h"
#include "exception/DuplicateSectionException.h"
#include "exception/InvalidOptionException.h"
#include "exception/InvalidSectionException.h"
#include "ini_utilities.h"
namespace ini_tools {

class IniConfig {
 public:
  IniConfig() = default;
  ~IniConfig() = default;

  /**
   * @brief 检查是否存在这个 Section.
   */
  bool hasSection(const std::string& sectionName) const;

  /**
   * @brief 检查是否存在这个 Option.
   */
  bool hasOption(const std::string& sectionName, const std::string& optionName) const;

  /**
   * @brief 获取对应的 Option.
   *
   * @param sectionName .
   * @param optionName  .
   * 如果找不到对应的sectionName 和 optionName, 将抛出异常 InvalidOptionException.
   */
  std::string getOption(const std::string& section_name, const std::string& option_name) const;

  void getOption(const std::string& section_name, const std::string& option_name, int& vel) const;

  void getOption(const std::string& section_name, const std::string& option_name, double& vel) const;

  void getOption(const std::string& section_name, const std::string& option_name, long& vel) const;

  void getOption(const std::string& section_name, const std::string& option_name, std::string& vel) const;

  void getOption(const std::string& section_name, const std::string& option_name, bool& vel) const;
  /**
   * @brief 获取所有 section name.
   */
  std::vector<std::string> sections() const;

  /**
   * @brief 获取对应 section下所有 option name.
   *
   * @param section_name .
   *
   * 如果不存在这个section, 则抛出异常 InvalidSectionException.
   */
  std::vector<std::string> options(const std::string& section_name) const;

  /**
   * @brief 设置 option 的值.
   *
   * 如果 section/option 不存在, 则抛出异常 InvalidOptionException.
   */
  void setOption(const std::string& section_name, const std::string& option_name, const std::string& option_value);

  /**
   * @brief 删除 section.
   *
   * 如果 section 不存在, 则抛出异常  InvalidSectionException.
   */
  void removeSection(const std::string& section_name);

  /**
   * @brief 删除 option.
   *
   * 如果 section/option 不存在, 则抛出异常 InvalidOptionException.
   */
  void removeOption(const std::string& section_name, const std::string& option_name);

  /**
   *  @brief  添加一个 section.
   *
   * 如果 section 已存在, 则抛出异常 DuplicateSectionException.
   */
  void addSection(const std::string& section_name);

  /**
   * @brief  添加一个 option .
   *
   * 如果不存在这个section, 则抛出异常 InvalidSectionException.
   * 如果 section 已存在, 则抛出异常 DuplicateOptionException.
   */
  void addOption(const std::string& section_name, const std::string& option_name, const std::string& option_value);

 private:
  /**
   * @brief 如果sectionName 不存在，则抛出异常 InvalidSectionName
   */
  void validateSection(const std::string& section_name) const;

  /**
   * @brief 如果sectionName/optionName 不存在，则抛出异常 InvalidOptionName
   */
  void validateOption(const std::string& section_name, const std::string& option_name) const;

  // INI文件内容的存储变量
  std::unordered_map<std::string, std::unordered_map<std::string, std::string>> section_key2value_;
};

}  // namespace ini_tools
