/*******************************************************************************
 * Copyright (c) 2022/09/11, Liao LunJia.
 * All rights reserved.
 *******************************************************************************/

#pragma once

#include <string>
namespace ini_tools {
namespace utils {
/**
 * @brief 修剪左边无用数据.
 */
std::string leftTrim(const std::string& str);

/**
 * @brief 修剪右边无用数据.
 */
std::string rightTrim(const std::string& str);

/**
 * @brief 修剪每行数据左右边无用数据.
 */
std::string trim(const std::string& str);

/**
 * @brief 判断是否是一个 section? section格式为 [section]:
 */
bool isSection(const std::string& str);

std::string parseSection(const std::string& str);

/**
 * @brief 判断是否是一个 option? option 格式为 key=value:
 */
bool isOption(const std::string& str);

/**
 * @brief 解析一个option ，这里已知option的格式是正确的.
 */
std::pair<std::string, std::string> parseOption(const std::string& str);

/**
 *  @brief 判断是否是一行注释? 注释的第一个符号是 ;
 */
bool isComment(const std::string& str);

/**
 *  @brief 判断是否是空行
 */
bool isEmptyLine(const std::string& str);

/**
 * @brief 创建一个 section
 */
std::string createSection(const std::string& section);

/**
 * @brief 创建一个 option
 */
std::string createOption(const std::string& optionName, const std::string& optionValue);
}  // namespace utils

}  // namespace ini_tools
