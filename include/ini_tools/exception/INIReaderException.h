/*******************************************************************************
 * Copyright (c) 2022/09/11, Liao LunJia.
 * All rights reserved.
 *******************************************************************************/
#pragma once
#include <stdexcept>

namespace ini_tools {

class INIReaderException : public std::exception {
 public:
  INIReaderException(const std::string msg) throw() : message_(msg) {}
  virtual ~INIReaderException() throw(){};

  virtual const char* what() const throw() { return message_.c_str(); }

 private:
  const std::string message_;
};

}  // namespace ini_tools
