/*******************************************************************************
 * Copyright (c) 2022/09/11, Liao LunJia.
 * All rights reserved.
 *******************************************************************************/
#pragma once

#include <stdexcept>

namespace ini_tools {

class InvalidOptionException : public std::exception {
 public:
  InvalidOptionException(const std::string msg) throw() : message_(msg) {}
  virtual ~InvalidOptionException() throw(){};

  virtual const char* what() const throw() { return message_.c_str(); }

 private:
  const std::string message_;
};

}  // namespace ini_tools
