/*******************************************************************************
 * Copyright (c) 2022/09/11, Liao LunJia.
 * All rights reserved.
 *******************************************************************************/
#pragma once
#include <string>

#include "ini_config.h"
#include "ini_utilities.h"
namespace ini_tools {

class IniParse {
 public:
  IniParse() = default;
  ~IniParse() = default;

  /**
   * @brief 将ini文件读取到IniConfig.
   *
   * 如果操作有问题, 将抛出异常 INIReaderException.
   */
  IniConfig read(const std::string& filename);

  /**
   * @brief 将config写进ini文件.
   *
   * 如果操作有问题, 将抛出异常 INIWriterException.
   */
  void write(IniConfig& ini_config, const std::string& filename);
};

}  // namespace ini_tools
