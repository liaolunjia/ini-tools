/*******************************************************************************
 * Copyright (c) 2022/09/11, Liao LunJia.
 * All rights reserved.
 *******************************************************************************/

#include "ini_tools/ini_utilities.h"

#include <string>
namespace ini_tools {
namespace utils {
const std::string TRIM_DELIMITERS = " \f\n\r\t\v";

std::string leftTrim(const std::string& str) {
  std::string::size_type s = str.find_first_not_of(TRIM_DELIMITERS);
  if (s == std::string::npos) {
    return "";
  }
  return str.substr(s);
}

std::string rightTrim(const std::string& str) {
  std::string::size_type s = str.find_last_not_of(TRIM_DELIMITERS);
  if (s == std::string::npos) {
    return "";
  }
  return str.substr(0, s + 1);
}

std::string trim(const std::string& str) { return rightTrim(leftTrim(str)); }

bool isSection(const std::string& str) {
  // Only perform a right trim
  std::string trimmedStr = trim(str);
  if (trimmedStr[0] == '[' && trimmedStr[trimmedStr.size() - 1] == ']') {
    return true;
  }
  return false;
}

std::string parseSection(const std::string& str) {
  std::string trimmedStr = trim(str);
  return trimmedStr.substr(1, trimmedStr.size() - 2);
}

bool isOption(const std::string& str) {
  std::string trimmedStr = trim(str);
  std::string::size_type s = trimmedStr.find_first_of("=");
  if (s == std::string::npos) {
    return false;
  }
  std::string key = trim(trimmedStr.substr(0, s));
  // key can't be empty
  if (key == "") {
    return false;
  }
  return true;
}

std::pair<std::string, std::string> parseOption(const std::string& str) {
  std::string trimmedStr = trim(str);
  std::string::size_type s = trimmedStr.find_first_of("=");
  std::string key = trim(trimmedStr.substr(0, s));
  std::string value = trim(trimmedStr.substr(s + 1));

  return std::make_pair(key, value);
}

bool isComment(const std::string& str) {
  std::string trimmedStr = leftTrim(str);
  if (trimmedStr[0] == ';') {
    return true;
  }
  return false;
}

bool isEmptyLine(const std::string& str) {
  std::string trimmedStr = leftTrim(str);
  if (trimmedStr.empty()) {
    return true;
  }
  return false;
}

std::string createSection(const std::string& section) { return "[" + section + "]"; }

std::string createOption(const std::string& optionName, const std::string& optionValue) {
  return optionName + "=" + optionValue;
}
}  // namespace utils
}  // namespace ini_tools
