/*******************************************************************************
 * Copyright (c) 2022/09/11, Liao LunJia.
 * All rights reserved.
 *******************************************************************************/

#include "ini_tools/ini_parse.h"

#include "ini_tools/exception/INIReaderException.h"
#include "ini_tools/exception/INIWriterException.h"
namespace ini_tools {
IniConfig IniParse::read(const std::string& filename) {
  std::ifstream is;
  is.open(filename.c_str());
  if (!is.is_open()) {
    std::string msg = "Unable to read " + filename;
    throw INIReaderException(msg);
  }

  IniConfig config;
  std::string section;
  try {
    std::string line;
    while (getline(is, line)) {
      if (utils::isEmptyLine(line) || utils::isComment(line)) {
        // ignore it
      } else if (utils::isSection(line)) {
        section = utils::parseSection(line);
        config.addSection(section);
      } else if (utils::isOption(line)) {
        std::pair<std::string, std::string> option = utils::parseOption(line);
        config.addOption(section, option.first, option.second);
      } else {
        std::string msg = "Invalid line: " + line;
        throw INIReaderException(msg.c_str());
      }
    }
    is.close();
  } catch (...) {
    is.close();
    throw;
  }
  return config;
}
void IniParse::write(IniConfig& ini_config, const std::string& filename) {
  std::ofstream os;
  os.open(filename.c_str());
  if (!os.is_open()) {
    std::string msg = "Unable to write " + filename;
    throw INIWriterException(msg);
  }

  try {
    const std::vector<std::string>& sections = ini_config.sections();
    for (const auto& section_item : sections) {
      os << utils::createSection(section_item) << std::endl;
      const std::vector<std::string>& opts = ini_config.options(section_item);
      for (const auto& option_item : opts) {
        std::string value = ini_config.getOption(section_item, option_item);
        os << utils::createOption(option_item, value) << std::endl;
      }
      os << std::endl;
    }
    os.close();
  } catch (...) {
    os.close();
    throw;
  }
}
}  // namespace ini_tools
