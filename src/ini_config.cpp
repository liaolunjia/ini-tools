/*******************************************************************************
 * Copyright (c) 2022/09/09, Liao LunJia.
 * All rights reserved.
 *******************************************************************************/

#include "ini_tools/ini_config.h"

namespace ini_tools {

bool IniConfig::hasSection(const std::string& sectionName) const {
  if (section_key2value_.find(sectionName) == section_key2value_.end()) {
    return false;
  }
  return true;
}

bool IniConfig::hasOption(const std::string& sectionName, const std::string& optionName) const {
  if (!hasSection(sectionName)) {
    return false;
  }
  const std::unordered_map<std::string, std::string>& options = section_key2value_.at(sectionName);
  if (options.find(optionName) == options.end()) {
    return false;
  }
  return true;
}

std::string IniConfig::getOption(const std::string& section_name, const std::string& option_name) const {
  validateOption(section_name, option_name);

  const std::unordered_map<std::string, std::string>& options = section_key2value_.at(section_name);
  return options.at(option_name);
}

void IniConfig::getOption(const std::string& section_name, const std::string& option_name, int& vel) const {
  std::string vel_str = getOption(section_name, option_name);
  vel = std::stoi(vel_str);
}

void IniConfig::getOption(const std::string& section_name, const std::string& option_name, double& vel) const {
  std::string vel_str = getOption(section_name, option_name);
  vel = std::stod(vel_str);
}

void IniConfig::getOption(const std::string& section_name, const std::string& option_name, long& vel) const {
  std::string vel_str = getOption(section_name, option_name);
  vel = std::stol(vel_str);
}

void IniConfig::getOption(const std::string& section_name, const std::string& option_name, std::string& vel) const {
  std::string vel_str = getOption(section_name, option_name);
  vel = vel_str;
}

void IniConfig::getOption(const std::string& section_name, const std::string& option_name, bool& vel) const {
  std::string vel_str = getOption(section_name, option_name);
  if (vel_str == "true") {
    vel = true;
  } else {
    vel = false;
  }
}

std::vector<std::string> IniConfig::sections() const {
  std::vector<std::string> sections;
  for (const auto& item : section_key2value_) {
    sections.push_back(item.first);
  }
  return sections;
}

std::vector<std::string> IniConfig::options(const std::string& section_name) const {
  validateSection(section_name);
  const std::unordered_map<std::string, std::string>& opts = section_key2value_.at(section_name);
  std::vector<std::string> options;
  for (const auto& item : opts) {
    options.push_back(item.first);
  }
  return options;
}

void IniConfig::setOption(const std::string& section_name, const std::string& option_name,
                          const std::string& option_value) {
  validateOption(section_name, option_name);

  std::unordered_map<std::string, std::string>& options = section_key2value_[section_name];
  options[option_name] = option_value;
}

void IniConfig::removeSection(const std::string& section_name) {
  validateSection(section_name);
  section_key2value_.erase(section_name);
}

void IniConfig::removeOption(const std::string& section_name, const std::string& option_name) {
  validateOption(section_name, option_name);
  section_key2value_[section_name].erase(option_name);
}

void IniConfig::addSection(const std::string& section_name) {
  if (hasSection(section_name)) {
    std::string msg = "Duplicate section (" + section_name + ") found";
    throw DuplicateSectionException(msg);
  }
  std::unordered_map<std::string, std::string> empty_map;
  section_key2value_[section_name] = empty_map;
}

void IniConfig::addOption(const std::string& section_name, const std::string& option_name,
                          const std::string& option_value) {
  validateSection(section_name);

  std::unordered_map<std::string, std::string>& options = section_key2value_[section_name];
  if (hasOption(section_name, option_name)) {
    std::string msg = "Duplicate option (" + option_name + ") found";
    throw DuplicateOptionException(msg);
  }
  options[option_name] = option_value;
}

void IniConfig::validateSection(const std::string& section_name) const {
  if (!hasSection(section_name)) {
    std::string msg = "Section (" + section_name + ") not found";
    throw InvalidSectionException(msg);
  }
}

void IniConfig::validateOption(const std::string& section_name, const std::string& option_name) const {
  if (!hasOption(section_name, option_name)) {
    std::string msg = "Section (" + section_name + "), Option (" + option_name + ") not found";
    throw InvalidOptionException(msg);
  }
}

}  // namespace ini_tools
